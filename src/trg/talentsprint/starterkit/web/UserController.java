package trg.talentsprint.starterkit.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import trg.talentsprint.starterkit.model.Employee;
import trg.talentsprint.starterkit.model.User;
import trg.talentsprint.starterkit.repository.EmployeeRepository;
import trg.talentsprint.starterkit.service.SecurityService;
import trg.talentsprint.starterkit.service.*;
import trg.talentsprint.starterkit.validator.UserValidator;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private EmployeeService EmployeeService;
	@Autowired
	private EmployeeRepository Erp;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private UserValidator userValidator;

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("userForm", new User());

		return "registration";
	}

	@PostMapping("/registration")
	public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
		userValidator.validate(userForm, bindingResult);

		if (bindingResult.hasErrors()) {
			return "registration";
		}

		userService.save(userForm);

		securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

		return "redirect:/welcome";
	}

	@GetMapping("/login")
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");

		return "login";
	}

	@GetMapping({ "/", "/welcome" })
	public String welcome(String user, Model model) {
		model.addAttribute("Employees",EmployeeService.findAll());
		// model.addAttribute("Employees",Erp.findByUser(user));
		return "welcome";
	}

	@GetMapping("/new-employee")
	public String showEmployeesCreationForm(Model model) {
		model.addAttribute("emp", new Employee());
		return "new-employee";
	}

	@PostMapping("/add")
	public String addNewEmployee(@Valid @ModelAttribute Employee emp, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "new-employee";
		}
		EmployeeService.save(emp);
		model.addAttribute("Employees", EmployeeService.findAll());
		return "welcome";
	}

	@GetMapping("/{id}")
	public String showBookdById(@PathVariable Long id, Model model) {
		Employee book = EmployeeService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		model.addAttribute("emp", book);
		return "edit-employee";
	}

	@PostMapping("/{id}/update")
	public String updateBook(@PathVariable Long id, @Valid @ModelAttribute Employee emp, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			return "edit-employee";
		}
		EmployeeService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		EmployeeService.save(emp);
		model.addAttribute("Employees", EmployeeService.findAll());
		return "welcome";
	}

	@GetMapping("/{id}/delete")
	public String deleteBook(@PathVariable Long id, Model model) {
		EmployeeService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		EmployeeService.deleteById(id);
		model.addAttribute("Employees", EmployeeService.findAll());
		return "welcome";
	}

	@RequestMapping("/search")
	public String searchByName(String name, String location, String user, Model model) {
		model.addAttribute("se", Erp.findByNameOrLocationOrUser(name, location, user));
		return "search";
		// ${se.name}

	}

	@GetMapping("/{user}/myemp")
	public String employeesByUser(@PathVariable String user, Model model) {
		model.addAttribute("emp", Erp.findByUser(user));
		return "myEmp";
	}
	@GetMapping("/home")
	public String homePage(Model model) {
		model.addAttribute("Employees",EmployeeService.findAll());
		return "welcome";
	}
	@GetMapping("/about")
	public String aboutCompany() {
		return "about";
	}

	@GetMapping("/services")
	public String servicesOfferedCompany() {
		return "services";
	}

	@GetMapping("/careers")
	public String careersOfferedByCompany() {
		return "careers";
	}

	@GetMapping("/About")
	public String employeeObjective() {
		return "About";
	}

	@GetMapping("/comments")
	public String employeeFeedBack() {
		return "comments";
	}
}
