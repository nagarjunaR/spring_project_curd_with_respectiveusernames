package trg.talentsprint.starterkit.repository;

import trg.talentsprint.starterkit.model.*;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	
	
	List<Employee> findByNameOrLocationOrUser(String name,String location,String user);
	List<Employee> findByUser(String user);
	/*
	 * @Query("select e from Employee e where e.name = ?1") Employee
	 * findByName(String name);
	 * 
	 * @Query("select e from Employee e where e.location = ?1") Employee
	 * findByLocation(String location);
	 */

	/*
	 * @Query(value = "select * from employee where name= ?1 or location=?2",
	 * nativeQuery = true) Employee findByNameOrLocation(String name,String
	 * location);
	 */
	
}
