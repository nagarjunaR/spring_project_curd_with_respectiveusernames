<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Search</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
th {
	background-color: yellow;
}
</style>
</head>
<body align="center">
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<div class="container-full">
		<nav class="navbar bg-secondary">
			<img src="${contextPath}/Images/logo.jpeg" class="rounded-circle"
				align="left" style="height: 120px; width: 120px" alt="not found">
			<h1>
				<i style="color: yellow;">List of Employees in "N.R Software
					Solutions"</i>
			</h1>
			<a href="${contextPath}/home"><input type="submit" name="action"
				class="btn btn-primary" value="Home"></a> <a
				href="${contextPath}/about"><input type="submit" name="action"
				class="btn btn-primary" value="About Us"></a> <a
				href="${contextPath}/services"><input type="submit"
				name="action" class="btn btn-primary" value="Services"></a> <a
				href="${contextPath}/careers"><input type="submit" name="action"
				class="btn btn-primary" value="Careers"></a> <img
				src="${contextPath}/Images/pic3.jpeg" class="rounded-circle"
				align="left" style="height: 120px; width: 120px" alt="not found">
		</nav>
	</div>
	<div class="container-full" align="right">
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>

			<h2>
				Welcome ${pageContext.request.userPrincipal.name} | <a
					onclick="document.forms['logoutForm'].submit()"
					style="color: blue;">Logout</a>
			</h2>
		</c:if>
		<hr />
	</div>
	<div align="center">
		<form action="${contextPath}/search" method="get">
			<input type="text" name="name" placeholder="Employee Name">&nbsp;
			<input type="text" name="location" placeholder="Employee Location">&nbsp;
			<input type="text" name="user" placeholder="User Name"> <input
				type="submit" class="btn btn-warning" value="search">
		</form>
	</div>

	<div align="left">
		<form
			action="${contextPath}/${pageContext.request.userPrincipal.name}/myemp">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button align="right" type="submit" class="btn btn-success"
				name="My Emp">My Employee</button>
		</form>
		<div>
			<h2 align="center" style="color: blue;">List of Employees
				Searched By You</h2>
			<div>
				<table style="background-color: cyan;" class="table"
					class="table table-hover" border="1">
					<tr>
						<th>User</th>
						<th>Id</th>
						<th>Name</th>
						<th>Location</th>
						<th>Salary</th>
						<th>Rating</th>
						<th>Feed back</th>
						<th>Edification</th>
					</tr>
					<c:forEach var="se" items="${emp}">
						<tr>
							<td>${se.user}<br></td>
							<td>${se.id}<br></td>
							<td>${se.name}<br></td>
							<td>${se.location}<br></td>
							<td>${se.salary}<br></td>
							<td>5</td>
							<td>
								<form action="${contextPath}/comments" method="get">
									<input type="submit" class="btn btn-primary" value="Comment" />
								</form>
							</td>
							<td><form action="${contextPath}/About" method="get">
									<input type="submit" class="btn btn-primary" value="About" />
								</form></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<img alt="Not Found" src="${contextPath}/Images/pic1.jpeg">&nbsp;&nbsp;&nbsp;
			<img alt="Not Found" src="${contextPath}/Images/pic2.jpeg">&nbsp;&nbsp;&nbsp;
			<img alt="Not Found" src="${contextPath}/Images/pic3.jpeg">&nbsp;&nbsp;&nbsp;
			<img alt="Not Found" src="${contextPath}/Images/pic4.jpeg">&nbsp;&nbsp;&nbsp;
			<img alt="Not Found" src="${contextPath}/Images/pic5.jpeg">&nbsp;&nbsp;&nbsp;
			<img alt="Not Found" src="${contextPath}/Images/pic6.jpeg">
</body>
</html>