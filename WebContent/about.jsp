<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Overview</h1>
	<h2>
		N.R Software Solutions is a global leader in next-generation digital
		services and consulting. We enable clients in 46 countries to navigate
		their digital transformation. <br>** With over three decades of
		experience in managing the systems and workings of global enterprises,
		we expertly steer our clients through their digital journey.<br>**
		We do it by enabling the enterprise with an AI-powered core that helps
		prioritize the execution of change. We also empower the business with
		agile digital at scale to deliver unprecedented levels of performance
		and customer delight. Our always-on learning agenda drives their
		continuous improvement through building and transferring digital
		skills, expertise, and ideas from our innovation ecosystem.
	</h2>
</body>
</html>