<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ page isELIgnored="false"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Book Library</title>
</head>
<body>
	<div class="container">
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>

			<h2>
				${pageContext.request.userPrincipal.name} | <a
					onclick="document.forms['logoutForm'].submit()"
					style="color: blue;">Logout</a>
			</h2>
		</c:if>
		<hr />
	</div>

	<div align="center">
		<h2>New Employee</h2>
		<div>
			<div>
				<form:form action="${contextPath}/${emp.id}/update"
					modelAttribute="emp" method="post">
					<div>
						<div>Id: ${emp.id}</div>
						<div>
							<form:label path="user">User</form:label>
							<form:input type="text" id="user" path="user"
								value="${pageContext.request.userPrincipal.name}" />
							<form:errors path="user" />
						</div>
						<div>
							<form:label path="name">Name</form:label>
							<form:input type="text" id="name" path="name" />
							<form:errors path="name" />
						</div>
						<div>
							<form:label path="location">Location</form:label>
							<form:input type="text" id="location" path="location" />
							<form:errors path="location" />
						</div>
						<div>
							<form:label path="salary">Salary</form:label>
							<form:input type="number" id="salary" path="salary" />
							<form:errors path="salary" />
						</div>
					</div>
					<div>
						<div>
							<input type="submit" value="Update Employee">
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>

</body>
</html>