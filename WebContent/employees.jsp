<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee List</title>
</head>
<body>
	
		<div>
			<div>
				<h2>List Of Employees in Company</h2>
				<hr />
				<input type="text" name="action"> <input type="submit"
					name="action"> <br> <br> <a href="/new-employee">
					<button type="submit">Add new employee</button>
				</a><br> <br>

				<div>
					<div>
						<div>Employee list</div>
					</div>
					<div>
						<table border="1" cellspacing="0px" cellpadding="20px">
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Location</th>
								<th>Salary</th>
								<th>Actions</th>
							</tr>
							<c:forEach var="emp" items="${employees}">
								<tr>
									<td>${emp.id}</td>
									<td>${emp.name}</td>
									<td>${emp.location}</td>
									<td>${emp.salary}</td>

									<td><a href="/${emp.id}">Edit</a>
										<form action="/${emp.id}/delete" method="post">
											<input type="submit" value="Delete" />
										</form></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</div>

</body>
</html>